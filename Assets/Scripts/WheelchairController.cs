using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class WheelchairController : MonoBehaviour
{

	private float horizontalInput;
	private float verticalInput;

    #region BaseParameters
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------BASE PARAMETERS---------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------

    [InfoBox("Ce sont les param�tres n�cessaires au bon fonctionnement du controller, ils ne doivent pas �tre chang�s", InfoMessageType.Error)]
	[Title("Param�tres initiaux", "A ne pas changer", TitleAlignments.Left)]
	[SceneObjectsOnly]
	[FoldoutGroup("BaseParameters")]
	public Rigidbody rightWheelR;

	[FoldoutGroup("BaseParameters")]
	[SceneObjectsOnly]
	public Rigidbody leftWheelR;

	[FoldoutGroup("BaseParameters")]
	[SceneObjectsOnly]
	public Rigidbody frame;

	[FoldoutGroup("BaseParameters")]
	[SceneObjectsOnly]
	public Transform wheelchairVisual;

	[FoldoutGroup("BaseParameters")]
	[EnumPaging]
	public OVRInput.Controller leftController;

	[EnumPaging]
	[FoldoutGroup("BaseParameters")]
	public OVRInput.Controller rightController;
    #endregion
    #region ThumbstickParameters
    //----------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------THUMBSTICK PARAMETERS--------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------
    [Title("Param�tres de contr�le au stick", "Valeurs modifiables")]
	[FoldoutGroup("ThumbstickController")]
	[InfoBox("Ces valeurs modifient le comportement du contr�le au Joystick, elles peuvent �tre chang�es", InfoMessageType.Warning)]
	[LabelText("Vitesse d'acc�l�ration au joystick")]
	[Range(0f,800f)]
	public float thumbstickAccelerationForce;

	[FoldoutGroup("ThumbstickController")]
	[LabelText("Vitesse de rotation au joystick")]
	[Range(0f, 800f)]
	public float thumbstickRotationForce;

	[FoldoutGroup("ThumbstickController")]
	[LabelText("Pourcentage de compensation de rotation")]
	[Tooltip("Lorsque la rotation s'effectue, c'est la roue oppos�e � la direction qu'on veut atteindre qui tourne, la valeurs ci-dessus permet de modifier le pourcentage de compensation" +
		"de l'autre roue pour tourner plus ou moins sur soi-m�me sans avancer")]
	[Range(0f, 200f)]
	public float thumbstickRotationCompensationPercentage;

	[LabelText("Palier du stick pour rotation")]
	[FoldoutGroup("ThumbstickController")]
	[Tooltip("Lorsqu'on penche le stick pour tourner, la rotation d�marre � partir du moment ou le stick atteint une certaine valeur horizontale" +
		"Cette valeur est modifiable via ce champ")]
	[Range(0.001f, 0.99f)]
	public float tresholdForSteering;

	[LabelText("Palier du stick pour recul")]
	[FoldoutGroup("ThumbstickController")]
	[Tooltip("Lorsqu'on pousse le stick vers l'arri�re pour reculer, la marche arri�re d�marre � partir du moment ou le stick atteint une certaine valeur verticale" +
		"Cette valeur est modifiable via ce champ")]
	[Range(0, -0.99f)]
	public float tresholdForBackwards;
#endregion
    #region ManualParameters
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------MANUAL PARAMETERS-------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------

    [Title("Param�tres de contr�les manuels", "Valeurs modifiables", TitleAlignments.Left, true, true)]
	[FoldoutGroup("ManualController")]
	[InfoBox("Ces valeurs modifient le comportement du contr�le avec les mains, elles peuvent �tre chang�es", InfoMessageType.Warning)]

	[LabelText("Force d'acc�l�ration manuelle")]
	[FoldoutGroup("ManualController")]
	[Tooltip("Force avec laquelle on pousse les roues pour l'acc�l�ration")]
	[Range(0f, 800f)]
	public float manualAccelerationForce;

	[Tooltip("Force avec laquelle on pousse les roues pour la rotation � deux mains")]
	[FoldoutGroup("ManualController")]
	[LabelText("Force de rotation manuelle � deux mains")]
	[Range(0f, 800f)]
	public float manualBothHandsRotationForce;

	[Tooltip("Force avec laquelle on pousse les roues pour la rotation � une main")]
	[FoldoutGroup("ManualController")]
	[LabelText("Force de rotation manuelle � une main")]
	[Range(0f, 800f)]
	public float manualSingleHandRotationForce;

	[Tooltip("Force de freinage : avec une valeur basse, le fauteuil mettra plus longtemps avant de freiner compl�tement")]
	[FoldoutGroup("ManualController")]
	[LabelText("Force des freins")]
	[Range(0f, 40f)]
	public float dragWhenBraking;

	[Tooltip("Force de frottement avec le sol : avec une valeur haute il sera plus difficile d'acc�l�rer")]
	[FoldoutGroup("ManualController")]
	[LabelText("Frottements des roues en avan�ant")]
	[Range(0f, 40f)]
	public float dragWhenAccelerating;

	[Tooltip("Force de frottement avec le sol : avec une valeur haute il sera plus difficile de tourner")]
	[FoldoutGroup("ManualController")]
	[LabelText("Frottements des roues en tournant")]
	[Range(0f, 40f)]
	public float dragWhenRotating;

	[Tooltip("Force de frottement avec le sol lorsqu'on l�che les roues")]
	[FoldoutGroup("ManualController")]
	[LabelText("Frottements des roues en l�chant les roues")]
	[Range(0f, 40f)]
	public float baseDrag;

	[Tooltip("Le freinage s'effectue lorsque les roues sont attrap�es et que les deux mains ne bougent plus sur les roues." +
		"Il est possible de freiner m�me en bougeant un peu les mains en modifiant cette valeur, par exemple si la personne tremble")]
	[FoldoutGroup("ManualController")]
	[LabelText("Palier de mouvement pour freinage")]
	[Range(0.001f, 2f)]
	public float handMovementTresholdForBraking;

	[FoldoutGroup("ManualController")]
	[LabelText("Epsilon acceleration")]
	[Range(0.001f, 2f)]
	public float epsilonForAcceleration;

	[FoldoutGroup("ManualController")]
	[LabelText("Epsilon rotation")]
	[Range(0.001f, 2f)]
	public float epsilonForRotation;


	#endregion
	#region DebugValues
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------DEBUG VALUES---------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------------
	[Title("Valeurs de debug", "Permettent le debug visuel", TitleAlignments.Left, true, true)]
	[FoldoutGroup("DebugValues")]
	[LabelText("Objet actuel tenu par la main gauche")]
	public GameObject leftHandCurrentTriggerObject;

	[LabelText("Objet actuel tenue par la main gauche")]
	[FoldoutGroup("DebugValues")]
	public GameObject rightHandCurrentTriggerObject;

	[LabelText("Roue gauche attrap�e")]
	[FoldoutGroup("DebugValues")]
	public bool leftHandIsGrabbingLeftWheel;

	[LabelText("Roue droite attrap�e")]
	[FoldoutGroup("DebugValues")]
	public bool rightHandIsGrabbingRightWheel;

	public enum WheelChairStates
	{
		acceleration,
		bothHandsRotation,
		singleHandRotation,
		handsFree,
		braking
	};

	[LabelText("Etat actuel du joueur")]
	[FoldoutGroup("DebugValues")]
	public WheelChairStates currentState;

	[FoldoutGroup("DebugValues")]
	[LabelText("Le fauteuil recul au stick")]
	public bool isGoingBackward;
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------------------------
    #endregion

    protected float leftHandForwardVelocity;
	protected float rightHandForwardVelocity;
	protected float averageHandsVelocity;


	private void Start()
    {
		InputReceiver.OnMoveEvent += GetInput;
		VrInteractionDetection.OnOtherTriggerEnter += OnInteractableTriggerEnter;
		VrInteractionDetection.OnOtherTriggerExit += OnInteractableTriggerExit;
		VrInteractionDetection.OnOtherGrabBegin += InteractableGrabBegin;
		VrInteractionDetection.OnOtherGrabEnd += InteractableGrabEnd;
	}

	private void FixedUpdate()
	{
        AccelerateThumbstick();
        //SteerThumbstick();
        WheelGrabHandling();
	}

	private void Update()
	{
		leftHandForwardVelocity = Mathf.Round(OVRInput.GetLocalControllerVelocity(leftController).z * 10f) / 10f;
		rightHandForwardVelocity = Mathf.Round(OVRInput.GetLocalControllerVelocity(rightController).z * 10f) / 10f;

		if (Input.GetKeyDown(KeyCode.K))
		{
			Debug.Log("+ de vitesse");
            rightWheelR.AddTorque(wheelchairVisual.right * manualAccelerationForce);
            leftWheelR.AddTorque(wheelchairVisual.right * manualAccelerationForce);
            //rightWheelR.angularVelocity = new Vector3(rightWheelR.angularVelocity.x + manualAccelerationForce, rightWheelR.angularVelocity.y + manualAccelerationForce, rightWheelR.angularVelocity.z + manualAccelerationForce);
            //rightWheelR.angularVelocity = new Vector3(leftWheelR.angularVelocity.x + manualAccelerationForce, leftWheelR.angularVelocity.y + manualAccelerationForce, leftWheelR.angularVelocity.z + manualAccelerationForce);
        }
		if (Input.GetKeyDown(KeyCode.L))
		{
			Debug.Log("- de vitesse");
            rightWheelR.AddTorque(-wheelchairVisual.right * manualAccelerationForce);
            leftWheelR.AddTorque(-wheelchairVisual.right * manualAccelerationForce);
            //rightWheelR.angularVelocity = new Vector3(rightWheelR.angularVelocity.x - manualAccelerationForce, rightWheelR.angularVelocity.y - manualAccelerationForce, rightWheelR.angularVelocity.z - manualAccelerationForce);
            //rightWheelR.angularVelocity = new Vector3(leftWheelR.angularVelocity.x - manualAccelerationForce, leftWheelR.angularVelocity.y - manualAccelerationForce, leftWheelR.angularVelocity.z - manualAccelerationForce);
        }
	}

    #region Hands movement

    public void OnInteractableTriggerEnter(OVRInput.Controller controller, GameObject otherObject)
	{
		if (controller == leftController)
			leftHandCurrentTriggerObject = otherObject;
		else
			rightHandCurrentTriggerObject = otherObject;
	}

	public void OnInteractableTriggerExit(OVRInput.Controller controller, GameObject otherObject)
	{
		if (controller == leftController)
			if (leftHandCurrentTriggerObject == otherObject)
				leftHandCurrentTriggerObject = null;
		if (controller == rightController)
			if (rightHandCurrentTriggerObject == otherObject)
				rightHandCurrentTriggerObject = null;
	}

	void InteractableGrabBegin(OVRInput.Controller controller)
	{

		if (leftHandCurrentTriggerObject != null && leftHandCurrentTriggerObject.CompareTag("LeftWheelchairTriggerZone") && controller == leftController)
		{
			leftHandIsGrabbingLeftWheel = true;
		}
		if (rightHandCurrentTriggerObject != null && rightHandCurrentTriggerObject.CompareTag("RightWheelchairTriggerZone") && controller == rightController)
		{
			rightHandIsGrabbingRightWheel = true;
		}
	}

	void InteractableGrabEnd(OVRInput.Controller controller)
	{
		if (controller == leftController)
		{
			leftHandIsGrabbingLeftWheel = false;
		}
		else
		{
			rightHandIsGrabbingRightWheel = false;
		}

		if (!leftHandIsGrabbingLeftWheel && !rightHandIsGrabbingRightWheel)
		{
			if (currentState != WheelChairStates.handsFree) Debug.Log(currentState);
			currentState = WheelChairStates.handsFree;
			leftWheelR.drag = baseDrag;
			rightWheelR.drag = baseDrag;
		}

	}

	void WheelGrabHandling()
    {
		if(leftHandIsGrabbingLeftWheel && rightHandIsGrabbingRightWheel)
        {

			//Acceleration
			if (Mathf.Sign(leftHandForwardVelocity) == Mathf.Sign(rightHandForwardVelocity) && Mathf.Abs(leftHandForwardVelocity + rightHandForwardVelocity) >= epsilonForAcceleration)
            {
				
				rightWheelR.drag = dragWhenAccelerating;
				leftWheelR.drag = dragWhenAccelerating;

				if (currentState != WheelChairStates.acceleration) Debug.Log(currentState);
				currentState = WheelChairStates.acceleration;
				averageHandsVelocity = leftHandForwardVelocity + rightHandForwardVelocity / 2;
				rightWheelR.AddTorque(wheelchairVisual.right * averageHandsVelocity * manualAccelerationForce);
				leftWheelR.AddTorque(wheelchairVisual.right * averageHandsVelocity * manualAccelerationForce);
			}
			//Rotation with both hands
            else if (Mathf.Sign(leftHandForwardVelocity) != Mathf.Sign(rightHandForwardVelocity) && (Mathf.Abs(leftHandForwardVelocity) + Mathf.Abs(rightHandForwardVelocity)) >= epsilonForRotation)
            {
				if (currentState != WheelChairStates.bothHandsRotation) Debug.Log(currentState);
				currentState = WheelChairStates.bothHandsRotation;
				Debug.Log(currentState);
				rightWheelR.drag = dragWhenRotating;
				leftWheelR.drag = dragWhenRotating;
				rightWheelR.AddTorque(wheelchairVisual.right * rightHandForwardVelocity * manualBothHandsRotationForce);
				leftWheelR.AddTorque(wheelchairVisual.right * leftHandForwardVelocity * manualBothHandsRotationForce);
			}
			//Braking
			else if (Mathf.Abs(leftHandForwardVelocity) < handMovementTresholdForBraking && Mathf.Abs(rightHandForwardVelocity) < handMovementTresholdForBraking)
			{
				if (currentState != WheelChairStates.braking) Debug.Log(currentState);
				currentState = WheelChairStates.braking;
				rightWheelR.drag = dragWhenBraking;
				leftWheelR.drag = dragWhenBraking;
			}
		}
        //Acceleration with only left wheel
        else if (leftHandIsGrabbingLeftWheel && !rightHandIsGrabbingRightWheel && currentState != WheelChairStates.acceleration)
        {
            currentState = WheelChairStates.singleHandRotation;
            rightWheelR.drag = dragWhenRotating;
            leftHandForwardVelocity = OVRInput.GetLocalControllerVelocity(leftController).z;
            leftWheelR.AddTorque(wheelchairVisual.right * leftHandForwardVelocity * manualSingleHandRotationForce);
        }
        //Acceleration with only right wheel
        else if (rightHandIsGrabbingRightWheel && !leftHandIsGrabbingLeftWheel && currentState != WheelChairStates.acceleration)
        {
            currentState = WheelChairStates.singleHandRotation;
            leftWheelR.drag = dragWhenRotating;
            rightHandForwardVelocity = OVRInput.GetLocalControllerVelocity(rightController).z;
            rightWheelR.AddTorque(wheelchairVisual.right * rightHandForwardVelocity * manualSingleHandRotationForce);
        }
    }
    #endregion

    #region Thumbstick movement
    public void GetInput(Vector2 inputValues)
	{
		horizontalInput = inputValues.x;
		verticalInput = inputValues.y;
		isGoingBackward = verticalInput < tresholdForBackwards;
	}
	private void SteerThumbstick()
	{
		
		if (Mathf.Abs(horizontalInput) > tresholdForSteering)
		{
			bool isGoingToRight = horizontalInput >= 0;
			Rigidbody forwardWheelR = isGoingToRight ? leftWheelR : rightWheelR;
			Rigidbody brakeWheelR = isGoingToRight ? rightWheelR : leftWheelR;
			forwardWheelR.AddTorque(wheelchairVisual.right * thumbstickRotationForce * Mathf.Abs(horizontalInput) * (isGoingBackward ? -1 : 1));
			brakeWheelR.AddTorque(-forwardWheelR.angularVelocity.normalized * thumbstickRotationForce * Mathf.Abs(horizontalInput) * (thumbstickRotationCompensationPercentage / 100));
		}

		
	}

	private void AccelerateThumbstick()
	{
		if(Mathf.Abs(verticalInput) > 0.01f)
        {
			leftWheelR.AddTorque(wheelchairVisual.right * (verticalInput * thumbstickAccelerationForce));
			rightWheelR.AddTorque(wheelchairVisual.right * (verticalInput * thumbstickAccelerationForce));
		}
		//Debug.DrawRay(wheelchairVisual.position, wheelchairVisual.forward, Color.red);
	}
    #endregion

}
