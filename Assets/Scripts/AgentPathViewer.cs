using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentPathViewer : MonoBehaviour
{
    private NavMeshAgent agent;

    public bool showPoints;
    public bool showPath;

    public Color pointColor = Color.red;
    public float pointSize = 1;
    
    public Color pathColor = Color.green;
    
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void OnDrawGizmos()
    {
        if (agent && agent.hasPath)
        {
            Vector3[] corners = agent.path.corners;
            Gizmos.color = pointColor;
            
            for (int i = 0; i < corners.Length; i++)
            {
                if (showPoints)
                    Gizmos.DrawSphere(corners[i], pointSize);

                if (showPath && i > 0)
                {
                    Debug.DrawLine(corners[i-1], corners[i], pathColor);
                }
            }
        }
    }
}
