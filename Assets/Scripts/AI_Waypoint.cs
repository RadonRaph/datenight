using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Waypoint : MonoBehaviour
{
    public float range = 2;
    public float alertMultiplier = 1;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
