using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject Player;
    [SerializeField] private Transform PlayerSpawnPosition;
    [SerializeField] private OVRScreenFade cameraFade;
    bool isEndingGame;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EndGame()
    {
        Debug.Log("OMG IL A APPUY�");
        if (!isEndingGame)
        {
            isEndingGame = true;
            cameraFade.FadeOut(() =>
            {
#if UNITY_EDITOR
                EditorApplication.isPlaying = false;
#else
            Application.Quit();            
#endif
            });
        }
    }
}
