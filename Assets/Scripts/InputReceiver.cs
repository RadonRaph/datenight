using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputReceiver : MonoBehaviour
{
    public delegate void MoveEvent(Vector2 moveValues);
    public static event MoveEvent OnMoveEvent;

    public delegate void GrabEvent();
    public static event GrabEvent OnGrabEvent;

    public delegate void ReleaseEvent();
    public static event ReleaseEvent OnReleaseEvent;

    private float previousHorizontalInput;
    private float previousVerticalInput;

    private Vector2 moveInputValuesBuffer;

    private void Start()
    {
        moveInputValuesBuffer = new Vector2(0, 0);
    }

    public void OnMove(Vector2 inputs)
    {
        OnMoveEvent?.Invoke(inputs);
    }

    private void OnGrab()
    {
        OnGrabEvent?.Invoke();
    }

    private void OnRelease()
    {
        OnReleaseEvent?.Invoke();
    }

    private void Update()
    {
        if (Input.GetAxis("Horizontal") != previousHorizontalInput || Input.GetAxis("Vertical") != previousVerticalInput)
        {
            previousHorizontalInput = moveInputValuesBuffer.x;
            previousVerticalInput = moveInputValuesBuffer.y;
            moveInputValuesBuffer.x = Input.GetAxis("Horizontal");
            moveInputValuesBuffer.y = Input.GetAxis("Vertical");
            OnMove(moveInputValuesBuffer);
        }

        if(OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > Mathf.Epsilon)
        {
            OnGrab();
        }
        else
        {
            OnRelease();
        }
    }
}
