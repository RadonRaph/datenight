using System;
using System.Collections;
using System.Collections.Generic;
using Autohand;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class BreakableObject : MonoBehaviour
{
    private Rigidbody _rb;
    private Rigidbody[] _rbs;
    private float _accumulatedForce;
    private Grabbable _grab;
    private Collider _collider;

    public float breakForce = 10f;

    public GameObject intactObject;
    public GameObject brokenObject;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _rbs = GetComponentsInChildren<Rigidbody>();
        _grab = GetComponent<Grabbable>();
        _collider = GetComponent<Collider>();
        

        for (int i = 0; i < _rbs.Length; i++)
        {
            _rbs[i].isKinematic = true;
        }

        _rb.isKinematic = false;
        
        intactObject.SetActive(true);
        brokenObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        float v = _rb.velocity.magnitude;

        if (v > 1f)
        {
            _accumulatedForce += v;
        }
        else
        {
            _accumulatedForce = 0;
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_accumulatedForce > breakForce)
        {
            Break();
        }
    }

    public void Break()
    {
        _rb.isKinematic = true;

        for (int i = 0; i < _rbs.Length; i++)
        {
            _rbs[i].isKinematic = false;
        }

        _grab.enabled = false;
        _collider.enabled = false;
        
        intactObject.SetActive(false);
        brokenObject.SetActive(true);
    }
}
