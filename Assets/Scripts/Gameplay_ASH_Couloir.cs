using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Raccoonlabs;
using DG.Tweening;

public class Gameplay_ASH_Couloir : MonoBehaviour
{
    public Transform Waypoint;

    public bool manualTrigger;

    public ASH_AI ASH;

    private NavMeshAgent _agent;
    private float baseSpeed;

    public Transform playerVisuals;

    public Transform mainDoor;
    public Transform secondDoor;
    
    // Start is called before the first frame update
    void Start()
    {
        _agent = ASH.gameObject.GetComponent<NavMeshAgent>();
        baseSpeed = _agent.speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (manualTrigger)
        {
            manualTrigger = false;
            OnTriggerEnter(null);
        }

        float d = Vector3.Distance(_agent.transform.position, playerVisuals.position);

        _agent.speed = (1-MathExtends.Remap(d, 1.5f, 10, 1, 0)) * baseSpeed;
    }

    private void OnTriggerEnter(Collider other)
    {
        Waypoint.position = transform.position;
        ASH.paused = false;
    }

    public void EndChapter()
    {
        ASH.paused = true;
        mainDoor.DOLocalRotate(Vector3.zero, 1f);
        secondDoor.DOLocalRotate(Vector3.zero, 1f);
    }
}
