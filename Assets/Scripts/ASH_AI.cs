using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class ASH_AI : MonoBehaviour
{
    private NavMeshAgent agent;
    
    public enum AIState
    {
        Idle,
        Alert,
        Chasing,
        Suspicious
    }

    public AIState currentState;

    public AI_Waypoint[] waypoints;
    public float waypointRange = 1;
    private int currentWaypoint = 0;
    private Vector3 waypointTarget;

    private Vector3 alertPoint;
    private Vector3 alertTarget;
    private float alertRange;
    private int alertIteration;
    private float alertTime;

    [Header("Targets")] 
    public Transform[] targets;
    private Transform closestTarget;
    public float lowRange;
    public float lowAngle;
    public float medRange;
    public float medAngle;
    public float hiRange;
    public float hiAngle;

    public LayerMask wallMask;

    private Transform chasingTarget;
    private Vector3 lastViewTarget;

    public float suspiciousTime;
    private Quaternion suspiciousRotation;

    public bool paused = true;

    private float angleMultiplier = 1;
    public enum AlertZone
    {
        None,
        Low,
        Medium,
        High
    }

    private AlertZone lastAlert;
    private AIState lastState;

    public UnityEvent OnAlert;
    public UnityEvent OnStartChasing;
    public UnityEvent OnEndChasing;
    public UnityEvent OnStartSuspicious;
    public UnityEvent OnEndSuspicious;
    private void OnDrawGizmos()
    {
        Vector3 position = transform.position;
        Vector3 right = transform.right;
        Vector3 forward = transform.forward;
        
        Vector3 lowOne = position + Vector3.RotateTowards((forward * lowRange), -right*lowRange,  Mathf.Deg2Rad*lowAngle*angleMultiplier, lowRange);
        Vector3 lowTwo = position + Vector3.RotateTowards((forward * lowRange), right*lowRange,  Mathf.Deg2Rad*lowAngle*angleMultiplier, lowRange);
        Debug.DrawLine(position, lowOne, Color.green);
        Debug.DrawLine(position, lowTwo, Color.green);
        
        Vector3 medOne = position + Vector3.RotateTowards((forward * medRange), -right*medRange,  Mathf.Deg2Rad*medAngle*angleMultiplier, medRange);
        Vector3 medTwo = position + Vector3.RotateTowards((forward * medRange), right*medRange,  Mathf.Deg2Rad*medAngle*angleMultiplier, medRange);
        Debug.DrawLine(position, medOne, Color.yellow);
        Debug.DrawLine(position, medTwo, Color.yellow);
        
        Vector3 hiOne = position + Vector3.RotateTowards((forward * hiRange), -right*hiRange,  Mathf.Deg2Rad*hiAngle*angleMultiplier, hiRange);
        Vector3 hiTwo = position + Vector3.RotateTowards((forward * hiRange), right*hiRange,  Mathf.Deg2Rad*hiAngle*angleMultiplier, hiRange);
        Debug.DrawLine(position, hiOne, Color.red);
        Debug.DrawLine(position, hiTwo, Color.red);
        
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(lastViewTarget, 0.1f);
    }

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        ChangeWaypoint();
    }

    // Update is called once per frame
    void Update()
    {
        if (paused)
            return;
    
        FindClosestTarget();
        
        CompareView();
        
        UpdateState();

        if (lastState != currentState)
        {
            if (lastState == AIState.Idle)
            {
                if (currentState == AIState.Alert || currentState == AIState.Chasing)
                {
                    OnAlert.Invoke();
                }
            }

            if (lastState != AIState.Suspicious && currentState == AIState.Suspicious)
            {
                OnStartSuspicious.Invoke();
            }

            if (lastState == AIState.Suspicious && currentState != AIState.Suspicious)
            {
                OnEndSuspicious.Invoke();
            }
            
            if (lastState != AIState.Chasing && currentState == AIState.Chasing)
            {
                OnStartChasing.Invoke();
            }

            if (lastState == AIState.Chasing && currentState != AIState.Chasing)
            {
                OnEndChasing.Invoke();
            }
            
            
            Debug.Log("AI Changing state from: " +lastState.ToString() + " to " + currentState.ToString());
        }

        lastState = currentState;
    }

    void FindClosestTarget()
    {
        float lowestDist = 9999;
        closestTarget = null;

        if (targets.Length > 1)
        {
            for (int i = 0; i < targets.Length; i++)
            {
                float d = Vector3.Distance(transform.position, targets[i].position);
                if (d < lowestDist && !Physics.Linecast(transform.position+Vector3.up*0.5f, targets[i].position, wallMask))
                {
                    lowestDist = d;
                    closestTarget = targets[i];
                }
            }
        }
        else
        {
            closestTarget = targets[0];
        }
    }

    void CompareView()
    {
        alertTime -= Time.deltaTime;

        if (waypoints.Length > 0)
        {
            int k = currentWaypoint - 1 < 0 ? waypoints.Length - 1 : currentWaypoint - 1;
            float L = Vector3.Distance(waypoints[k].transform.position, waypoints[currentWaypoint].transform.position);
            float l = Vector3.Distance(transform.position, waypoints[currentWaypoint].transform.position);
            angleMultiplier = Mathf.Lerp(waypoints[k].alertMultiplier, waypoints[currentWaypoint].alertMultiplier,
                1 - l / L);
        }

        if (alertTime > 0 || closestTarget == null)
            return;

        alertTime = 0.5f;
        
        AlertZone currentZone = AlertZone.None;

        float distance = Vector3.Distance(transform.position, closestTarget.position);
        float angle = Vector3.Angle(transform.forward, closestTarget.position - transform.position);
        


        float alertThre = 0;

        if (distance < hiRange && angle < hiAngle*angleMultiplier)
        {
            currentZone = AlertZone.High;
            alertThre = 1;
        }else if (distance < medRange && angle < medAngle*angleMultiplier)
        {
            currentZone = AlertZone.Medium;
            alertThre = 0.33f;
        }else if (distance < lowRange && angle < lowAngle*angleMultiplier)
        {
            currentZone = AlertZone.Low;
            alertThre = 0.15f;
        }

        if (currentZone != lastAlert)
        {
            lastAlert = currentZone;
            if (Random.value < alertThre)
            {
                currentState = AIState.Chasing;
                chasingTarget = closestTarget;
                //lastViewTarget = chasingTarget.position;
            }
        }


    }

    void UpdateState()
    {
        gameObject.name = "ASH: " + currentState.ToString();
        switch (currentState)
        {
            case AIState.Idle:
                float dist = Vector3.Distance(transform.position, waypointTarget);
                if (dist > waypointRange)
                {
                    agent.SetDestination(waypointTarget);
                }
                else
                {
                    ChangeWaypoint();
                }
                break;
            case AIState.Alert:
                if (Vector3.Distance(transform.position, alertTarget) <= 1)
                {
                    if (alertIteration-1 < 0)
                    {
                        FindClosestWaypoint(transform.position);
                        currentState = AIState.Idle;
                    }
                    else
                    {
                        Alert();
                    }
                }
                else
                {
                    agent.SetDestination(alertTarget);
                }
                break;
            case AIState.Chasing:

                Debug.DrawLine(transform.position+Vector3.up*0.5f, chasingTarget.position, Color.red);
                if (Physics.Linecast(transform.position + Vector3.up*0.5f, chasingTarget.position, wallMask))
                {
                    Debug.Log("Chase lost");
                    currentState = AIState.Suspicious;
                    suspiciousTime = Random.Range(2, 7);
                    suspiciousRotation = transform.rotation;
                }
                else
                {
                    lastViewTarget = Vector3.Lerp(lastViewTarget, chasingTarget.position, 0.01f);
                    Vector3 _lock=transform.eulerAngles;
                    transform.LookAt(lastViewTarget);
                    Vector3 axis=transform.eulerAngles;
                    _lock.y = axis.y;
                    transform.eulerAngles = _lock;
                }

                agent.SetDestination(lastViewTarget);
                
                break;
            case AIState.Suspicious:
                if (suspiciousTime <= 0)
                    currentState = AIState.Idle;

                if (Quaternion.Angle(transform.rotation, suspiciousRotation) <= 0.1f)
                {
                    suspiciousRotation = transform.rotation * Quaternion.Euler(0, Random.Range(-360, 360), 0);
                }

                transform.rotation = Quaternion.Slerp(transform.rotation, suspiciousRotation, 3f*Time.deltaTime);

                suspiciousTime -= Time.deltaTime;
                
                break;
                
        }
    }

    void FindClosestWaypoint(Vector3 pos)
    {
        int closest = 0;
        for (int i = 1; i < waypoints.Length; i++)
        {
            if (Vector3.Distance(pos, waypoints[i].transform.position) < Vector3.Distance(pos, waypoints[closest].transform.position))
            {
                closest = i;
            }
        }

        currentWaypoint = closest;
    }

    void ChangeWaypoint()
    {
        if (waypoints.Length==0)
            return;
        
        currentWaypoint = (currentWaypoint + 1) % waypoints.Length;
        float range = waypoints[currentWaypoint].range;
        waypointTarget = waypoints[currentWaypoint].transform.position +
                         new Vector3(Random.Range(-range, range), 0, Random.Range(-range, range));
    }

    public void SetAlert(Vector3 position, float range)
    {
        currentState = AIState.Alert;
        alertIteration = 3;
        alertPoint = position;
        alertRange = range;
        Alert();
    }

    void Alert()
    {
        Vector3 pos = alertPoint + Random.insideUnitSphere * alertRange; 
        NavMeshHit hit;

        if (NavMesh.SamplePosition(pos, out hit, alertRange, NavMesh.AllAreas))
        {
            alertIteration--;
            alertTarget = hit.position;
            agent.SetDestination(alertTarget);
        }
        else
        {
            Debug.Log("No hit");
        }
    }
    
    
}
