using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alert_Caller : MonoBehaviour
{
    public bool CastAlert;
    public ASH_AI ASH;
    public float range = 10;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CastAlert)
        {
            ASH.SetAlert(transform.position, range);
            CastAlert = false;
        }
    }
}
