using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemToggle : MonoBehaviour
{
    private ParticleSystem _system;

    public bool state = false;
    
    // Start is called before the first frame update
    void Start()
    {
        _system = GetComponent<ParticleSystem>();
        UpdateSystem();
    }

    void UpdateSystem()
    {
        if (state == true && !_system.isEmitting)
        {
            _system.Play();
        }

        if (state == false && _system.isEmitting)
        {
            _system.Stop();
        }
    }

    private void Update()
    {
        UpdateSystem();
    }

    public void Toggle()
    {
        state = !state;
        UpdateSystem();
    }

    public void SetActive(bool s)
    {
        state = s;
        UpdateSystem();
    }
}
