// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "obj_toon_sahder"
{
	Properties
	{
		_ASEOutlineWidth( "Outline Width", Float ) = 0.001
		_ASEOutlineColor( "Outline Color", Color ) = (0,0,0,0)
		_toonRamp("toonRamp", 2D) = "white" {}
		_RangeLightsAndShadows("RangeLightsAndShadows", Float) = 0.5
		_normalMap("normalMap", 2D) = "bump" {}
		_albedo("albedo", 2D) = "white" {}
		_AlbedoTint("AlbedoTint", Color) = (0.509434,0.509434,0.509434,0)
		_RimOffset("RimOffset", Float) = 0.6
		_RimGradient("RimGradient", Range( 0 , 1)) = 0
		_RimTint("RimTint", Color) = (1,0,0,0)
		_min("min", Float) = 500
		_specCoverage("specCoverage", Float) = 12.5
		_max("max", Float) = 500
		_specularIntensity("specularIntensity", Range( 0 , 1)) = 1
		_specColor("specColor", Color) = (0,0,0,0)
		_specTransition("specTransition", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog  keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		
		float4 _ASEOutlineColor;
		float _ASEOutlineWidth;
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += ( v.normal * _ASEOutlineWidth );
		}
		inline half4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return half4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			o.Emission = _ASEOutlineColor.rgb;
			o.Alpha = 1;
		}
		ENDCG
		

		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "DisableBatching" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityCG.cginc"
		#include "UnityShaderVariables.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _AlbedoTint;
		uniform sampler2D _albedo;
		uniform float4 _albedo_ST;
		uniform sampler2D _toonRamp;
		uniform sampler2D _normalMap;
		uniform float4 _normalMap_ST;
		uniform float _RangeLightsAndShadows;
		uniform float _RimOffset;
		uniform float _RimGradient;
		uniform float4 _RimTint;
		uniform float _min;
		uniform float _max;
		uniform float _specCoverage;
		uniform float4 _specColor;
		uniform float _specTransition;
		uniform float _specularIntensity;

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_albedo = i.uv_texcoord * _albedo_ST.xy + _albedo_ST.zw;
			float4 albedo28 = ( _AlbedoTint * tex2D( _albedo, uv_albedo ) );
			float2 uv_normalMap = i.uv_texcoord * _normalMap_ST.xy + _normalMap_ST.zw;
			float3 normal22 = UnpackNormal( tex2D( _normalMap, uv_normalMap ) );
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult4 = dot( (WorldNormalVector( i , normal22 )) , ase_worldlightDir );
			float normalLightDir9 = dotResult4;
			float2 temp_cast_2 = ((normalLightDir9*_RangeLightsAndShadows + _RangeLightsAndShadows)).xx;
			float4 temp_output_30_0 = ( albedo28 * tex2D( _toonRamp, temp_cast_2 ) );
			float4 shadow15 = temp_output_30_0;
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			UnityGI gi37 = gi;
			float3 diffNorm37 = WorldNormalVector( i , normal22 );
			gi37 = UnityGI_Base( data, 1, diffNorm37 );
			float3 indirectDiffuse37 = gi37.indirect.diffuse + diffNorm37 * 0.0001;
			float4 temp_cast_3 = (( ase_lightAtten * 100.0 )).xxxx;
			float div111=256.0/float(250);
			float4 posterize111 = ( floor( temp_cast_3 * div111 ) / div111 );
			float clampResult100 = clamp( ( length( posterize111 ) - 30.0 ) , 0.0 , 1.0 );
			float4 lighting36 = ( shadow15 * ( ase_lightColor * float4( ( indirectDiffuse37 + clampResult100 ) , 0.0 ) ) );
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult7 = dot( normalize( (WorldNormalVector( i , normal22 )) ) , ase_worldViewDir );
			float normalViewDir10 = dotResult7;
			float4 rim51 = ( saturate( ( pow( ( 1.0 - saturate( ( _RimOffset + normalViewDir10 ) ) ) , _RimGradient ) * ( normalLightDir9 * ase_lightAtten ) ) ) * ( ase_lightColor * _RimTint ) );
			float dotResult70 = dot( ( ase_worldViewDir + _WorldSpaceLightPos0.xyz ) , normalize( (WorldNormalVector( i , normal22 )) ) );
			float smoothstepResult73 = smoothstep( _min , _max , pow( dotResult70 , _specCoverage ));
			float4 lerpResult89 = lerp( _specColor , ase_lightColor , _specTransition);
			float4 specular80 = ( ( ( smoothstepResult73 * lerpResult89 ) * _specularIntensity ) * ase_lightAtten );
			c.rgb = ( ( lighting36 + rim51 ) + specular80 ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float2 uv_albedo = i.uv_texcoord * _albedo_ST.xy + _albedo_ST.zw;
			float4 albedo28 = ( _AlbedoTint * tex2D( _albedo, uv_albedo ) );
			float4 temp_output_123_0 = albedo28;
			o.Albedo = temp_output_123_0.rgb;
			float4 color125 = IsGammaSpace() ? float4(0.3436276,0.4320954,0.6226415,0) : float4(0.0967288,0.1564832,0.3456162,0);
			o.Emission = ( albedo28 * color125 ).rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows exclude_path:deferred 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
1976;32;1857;929;312.5568;419.1403;1.716915;True;True
Node;AmplifyShaderEditor.CommentaryNode;41;-4717.566,56.46084;Inherit;False;657.5933;280;normal map;2;21;22;normal map;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;21;-4667.566,106.4608;Inherit;True;Property;_normalMap;normalMap;2;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;22;-4283.973,107.5373;Inherit;False;normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;23;-3954.602,433.7685;Inherit;False;22;normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;12;-3732.075,388.6139;Inherit;False;806.5105;430;normal.view.dir;4;8;7;6;10;normal.view.dir;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;6;-3682.075,438.6139;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;8;-3675.075,630.6135;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;11;-3792.073,-186.573;Inherit;False;1078;454.9423;Normal.light;4;3;5;4;9;Normal.light;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;24;-3991.571,-135.0621;Inherit;False;22;normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;7;-3370.075,529.6138;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;5;-3741.073,24.36928;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;3;-3704.677,-136.573;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;4;-3329.073,-101.6307;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;29;-2318.884,-326.4286;Inherit;False;862.639;473.6707;albedo;4;25;26;27;28;albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.LightAttenuation;97;-3095.831,1438.932;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;10;-3149.564,524.226;Inherit;False;normalViewDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;61;-3073.564,1879.226;Inherit;False;1974.222;683.3596;rimLight;17;51;53;55;54;56;46;44;45;47;48;49;50;60;57;58;59;63;RimLight;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;26;-2192.245,-276.4286;Inherit;False;Property;_AlbedoTint;AlbedoTint;4;0;Create;True;0;0;0;False;0;False;0.509434,0.509434,0.509434,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;20;-2452.966,331.0653;Inherit;False;1377.259;512.5387;shadow;10;15;31;30;13;19;14;18;115;116;117;shadow;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;9;-2938.073,-103.6307;Inherit;False;normalLightDir;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;25;-2268.884,-82.75797;Inherit;True;Property;_albedo;albedo;3;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;86;-3145.4,2672.504;Inherit;False;3049.303;1207.063;spec;22;83;80;78;79;76;84;77;89;73;75;90;74;87;88;71;70;72;66;68;67;64;69;spec;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-2988.399,1929.226;Inherit;False;Property;_RimOffset;RimOffset;5;0;Create;True;0;0;0;False;0;False;0.6;10.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;44;-3023.564,2077.189;Inherit;False;10;normalViewDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;101;-2946.685,1275.67;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;100;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;69;-3095.4,3129.504;Inherit;False;22;normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;64;-3035.4,2722.504;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.PosterizeNode;111;-2826.101,1404.301;Inherit;False;250;2;1;COLOR;0,0,0,0;False;0;INT;250;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;45;-2771.399,1973.226;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-2403.966,704.6037;Inherit;False;Property;_RangeLightsAndShadows;RangeLightsAndShadows;1;0;Create;True;0;0;0;False;0;False;0.5;4.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;13;-2379.461,569.0649;Inherit;False;9;normalLightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;67;-3095.4,2919.504;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-1895.244,-130.4287;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldNormalVector;68;-2823.4,3125.504;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ScaleAndOffsetNode;18;-2048.965,626.6037;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;28;-1680.244,-135.4286;Inherit;False;albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;43;-2448.098,1027.637;Inherit;False;1394.95;414.7863;lighting;9;34;37;40;38;39;42;33;35;36;lighting;1,1,1,1;0;0
Node;AmplifyShaderEditor.LengthOpNode;112;-2642.271,1432.612;Inherit;False;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;66;-2749.4,2816.504;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;47;-2566.399,1978.226;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;113;-2453.687,1483.396;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;30;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;40;-2398.098,1292.424;Inherit;False;22;normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;14;-1841.707,583.3546;Inherit;True;Property;_toonRamp;toonRamp;0;0;Create;True;0;0;0;False;0;False;-1;None;fd68bdcc37e59784a83e37ec1c26d400;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightAttenuation;59;-2597.625,2372.259;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;57;-2597.131,2255.677;Inherit;False;9;normalLightDir;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;31;-1743.228,433.3383;Inherit;False;28;albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;48;-2381.399,1980.226;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;50;-2520.399,2098.226;Inherit;False;Property;_RimGradient;RimGradient;6;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;70;-2474.4,2900.504;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;72;-2490.4,3095.504;Inherit;False;Property;_specCoverage;specCoverage;9;0;Create;True;0;0;0;False;0;False;12.5;16.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-1513.228,527.3382;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;87;-1918.103,3394.115;Inherit;False;Property;_specColor;specColor;13;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.9964047,0.5518868,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;74;-2263.4,3051.504;Inherit;False;Property;_min;min;8;0;Create;True;0;0;0;False;0;False;500;500;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-2273.601,2290.069;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;90;-1989.103,3753.115;Inherit;False;Property;_specTransition;specTransition;14;0;Create;True;0;0;0;False;0;False;0;0.21;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;49;-2200.399,1980.226;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;100;-2108.811,1481.721;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;88;-1866.103,3608.115;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.IndirectDiffuseLighting;37;-2166.098,1255.424;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;75;-2259.4,3132.504;Inherit;False;Property;_max;max;10;0;Create;True;0;0;0;False;0;False;500;500;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;71;-2282.4,2910.504;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;73;-1995.401,2910.504;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;34;-2160.149,1077.637;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RegisterLocalVarNode;15;-1289.706,523.3547;Inherit;False;shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-1924.099,1323.424;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;53;-1976.855,2218.586;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-1958.178,1986.292;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;89;-1590.103,3488.115;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;55;-2028.855,2350.586;Inherit;False;Property;_RimTint;RimTint;7;0;Create;True;0;0;0;False;0;False;1,0,0,0;1,0.7533273,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-1764.855,2269.586;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;77;-1217.203,3027.253;Inherit;False;Property;_specularIntensity;specularIntensity;11;0;Create;True;0;0;0;False;0;False;1;0.194;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;33;-1701.303,1130.231;Inherit;False;15;shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;84;-1522.073,2934.302;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;63;-1731.354,2003.63;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-1684.8,1243.153;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;56;-1553.32,1979.657;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-1477.148,1174.039;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;-915.6191,2919.309;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightAttenuation;79;-734.0146,3055.38;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;51;-1327.133,1967.89;Inherit;False;rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-508.255,2923.312;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;36;-1246.58,1169.039;Float;False;lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;16;375.238,110.086;Inherit;False;36;lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;80;-312.6803,2918.748;Inherit;False;specular;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;52;374.491,221.236;Inherit;False;51;rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;81;355.991,372.7217;Inherit;False;80;specular;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;125;683.9576,-196.9472;Inherit;False;Constant;_Color1;Color 1;15;0;Create;True;0;0;0;False;0;False;0.3436276,0.4320954,0.6226415,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;123;395.646,-87.16431;Inherit;False;28;albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;62;583.0709,156.9813;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;91;-3775.789,190.5887;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;83;-1877.377,3156.318;Inherit;True;Property;_TextureSample0;Texture Sample 0;12;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;993.9576,-237.9472;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;118;-1533.774,139.5119;Inherit;False;Constant;_Color0;Color 0;15;0;Create;True;0;0;0;False;0;False;0.06852973,0.1578007,0.2641509,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-1506.774,391.5119;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;85;775.5083,224.7783;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;117;-1281.774,390.5119;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightAttenuation;38;-2202.098,1350.424;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;116;-1721.774,355.5119;Inherit;False;Constant;_Float0;Float 0;15;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1184.311,0.5693817;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;obj_toon_sahder;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;True;0.001;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;22;0;21;0
WireConnection;6;0;23;0
WireConnection;7;0;6;0
WireConnection;7;1;8;0
WireConnection;3;0;24;0
WireConnection;4;0;3;0
WireConnection;4;1;5;0
WireConnection;10;0;7;0
WireConnection;9;0;4;0
WireConnection;101;0;97;0
WireConnection;111;1;101;0
WireConnection;45;0;46;0
WireConnection;45;1;44;0
WireConnection;27;0;26;0
WireConnection;27;1;25;0
WireConnection;68;0;69;0
WireConnection;18;0;13;0
WireConnection;18;1;19;0
WireConnection;18;2;19;0
WireConnection;28;0;27;0
WireConnection;112;0;111;0
WireConnection;66;0;64;0
WireConnection;66;1;67;1
WireConnection;47;0;45;0
WireConnection;113;0;112;0
WireConnection;14;1;18;0
WireConnection;48;0;47;0
WireConnection;70;0;66;0
WireConnection;70;1;68;0
WireConnection;30;0;31;0
WireConnection;30;1;14;0
WireConnection;58;0;57;0
WireConnection;58;1;59;0
WireConnection;49;0;48;0
WireConnection;49;1;50;0
WireConnection;100;0;113;0
WireConnection;37;0;40;0
WireConnection;71;0;70;0
WireConnection;71;1;72;0
WireConnection;73;0;71;0
WireConnection;73;1;74;0
WireConnection;73;2;75;0
WireConnection;15;0;30;0
WireConnection;39;0;37;0
WireConnection;39;1;100;0
WireConnection;60;0;49;0
WireConnection;60;1;58;0
WireConnection;89;0;87;0
WireConnection;89;1;88;0
WireConnection;89;2;90;0
WireConnection;54;0;53;0
WireConnection;54;1;55;0
WireConnection;84;0;73;0
WireConnection;84;1;89;0
WireConnection;63;0;60;0
WireConnection;42;0;34;0
WireConnection;42;1;39;0
WireConnection;56;0;63;0
WireConnection;56;1;54;0
WireConnection;35;0;33;0
WireConnection;35;1;42;0
WireConnection;76;0;84;0
WireConnection;76;1;77;0
WireConnection;51;0;56;0
WireConnection;78;0;76;0
WireConnection;78;1;79;0
WireConnection;36;0;35;0
WireConnection;80;0;78;0
WireConnection;62;0;16;0
WireConnection;62;1;52;0
WireConnection;124;0;123;0
WireConnection;124;1;125;0
WireConnection;115;0;31;0
WireConnection;115;1;116;0
WireConnection;85;0;62;0
WireConnection;85;1;81;0
WireConnection;117;0;115;0
WireConnection;117;1;118;0
WireConnection;117;2;30;0
WireConnection;0;0;123;0
WireConnection;0;2;124;0
WireConnection;0;13;85;0
ASEEND*/
//CHKSM=78B934FCB03E7B30FC87E1C83C846AC224D9B14D