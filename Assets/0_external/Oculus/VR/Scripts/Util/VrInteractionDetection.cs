using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrInteractionDetection : MonoBehaviour
{



    [SerializeField]
    protected LayerMask grabbableLayers;
    // Should be OVRInput.Controller.LTouch or OVRInput.Controller.RTouch.
    [SerializeField]
    protected OVRInput.Controller m_controller;

    // Grip trigger thresholds for picking up objects, with some hysteresis.
    public float grabBegin = 0.55f;
    public float grabEnd = 0.35f;

    protected float m_prevFlex;

    public delegate void GrabbableTriggerEnterEvent(OVRInput.Controller m_controller, OVRGrabbable grabbableObject);
    public static event GrabbableTriggerEnterEvent OnGrabbableTriggerEnter;

    public delegate void GrabbableTriggerExitEvent(OVRInput.Controller m_controller, OVRGrabbable grabbableObject);
    public static event GrabbableTriggerExitEvent OnGrabbableTriggerExit;

    public delegate void OtherTriggerEnterEvent(OVRInput.Controller m_controller, GameObject otherObject);
    public static event OtherTriggerEnterEvent OnOtherTriggerEnter;

    public delegate void OtherTriggerExitEvent(OVRInput.Controller m_controller, GameObject otherObject);
    public static event OtherTriggerExitEvent OnOtherTriggerExit;

    public delegate void GrabbableGrabBeginEvent(OVRInput.Controller m_controller);
    public static event GrabbableGrabBeginEvent OnGrabbableGrabBegin;

    public delegate void GrabbableGrabEndEvent(OVRInput.Controller m_controller);
    public static event GrabbableGrabEndEvent OnGrabbableGrabEnd;

    public delegate void OtherGrabBeginEvent(OVRInput.Controller m_controller);
    public static event OtherGrabBeginEvent OnOtherGrabBegin;

    public delegate void OtherGrabEndEvent(OVRInput.Controller m_controller);
    public static event OtherGrabEndEvent OnOtherGrabEnd;

    protected Collider otherObjectCollider;
    protected Collider grabbableObjectCollider;


    void OnTriggerEnter(Collider otherCollider)
    {
        if ((grabbableLayers.value & (1 << otherCollider.gameObject.layer)) > 0)
        {
            // Get the grab trigger
            OVRGrabbable grabbable = otherCollider.GetComponent<OVRGrabbable>() ?? otherCollider.GetComponentInParent<OVRGrabbable>();
            if (grabbable == null)
            {
                
                OnOtherTriggerEnter?.Invoke(m_controller, otherCollider.gameObject);
                otherObjectCollider = otherCollider;
            }
            else
            {
                OnGrabbableTriggerEnter?.Invoke(m_controller, grabbable);
                grabbableObjectCollider = otherCollider;
            }
        }
    }

    void OnTriggerExit(Collider otherCollider)
    {
        OVRGrabbable grabbable = otherCollider.GetComponent<OVRGrabbable>() ?? otherCollider.GetComponentInParent<OVRGrabbable>();
        if (grabbable == null)
        {
            OnOtherTriggerExit?.Invoke(m_controller, otherCollider.gameObject);
            if (otherObjectCollider == otherCollider)
                otherObjectCollider = null;
        }
        else
        {
            OnGrabbableTriggerExit?.Invoke(m_controller, grabbable);
            if (grabbableObjectCollider == otherCollider)
                grabbableObjectCollider = null;
        }
        
    }


    protected void CheckForGrabOrRelease(float prevFlex)
    {
        if ((m_prevFlex >= grabBegin))
        {
            if(grabbableObjectCollider != null)
                OnGrabbableGrabBegin?.Invoke(m_controller);
            else if(otherObjectCollider != null)
                OnOtherGrabBegin?.Invoke(m_controller);
        }
        else if ((m_prevFlex <= grabEnd))
        {
            OnGrabbableGrabEnd?.Invoke(m_controller);
            OnOtherGrabEnd?.Invoke(m_controller);
        }
    }

    private void Update()
    {
        float prevFlex = m_prevFlex;
        // Update values from inputs
        m_prevFlex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

        CheckForGrabOrRelease(prevFlex);
    }
}
