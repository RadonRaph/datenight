using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class OnTriggerEvent : MonoBehaviour
{


    public UnityEvent enterEvent;
    public UnityEvent exitEvent;

    public bool filterByTag;
    public string tagName;
    
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Collider>().isTrigger = true;
        GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (filterByTag)
        {
            if (other.gameObject.CompareTag(tagName))
                enterEvent.Invoke();
        }
        else
        {
            enterEvent.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (filterByTag)
        {
            if (other.gameObject.CompareTag(tagName))
                exitEvent.Invoke();
        }
        else
        {
            exitEvent.Invoke();
        }
    }
}
