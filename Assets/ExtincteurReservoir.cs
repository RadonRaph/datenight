using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtincteurReservoir : MonoBehaviour
{
    public float amount = 3;

    private ParticleSystemToggle _systemToggle;
    // Start is called before the first frame update
    void Start()
    {
        _systemToggle = GetComponent<ParticleSystemToggle>();
    }



    // Update is called once per frame
    void Update()
    {
        if (_systemToggle.state)
        {

            amount -= Time.deltaTime;

            if (amount <= 0)
            {
                _systemToggle.Toggle();
            }
        }
    }
}
