/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PAUSE_ALL = 3864097025U;
        static const AkUniqueID PLAY_AMB_DOG_BARKS = 194770229U;
        static const AkUniqueID PLAY_AMB_ROOMTONE_CHAMBER = 2775853383U;
        static const AkUniqueID PLAY_AMB_ROOMTONE_CORRIDOR = 3785183433U;
        static const AkUniqueID PLAY_AMB_ROOMTONE_DESK = 2173793916U;
        static const AkUniqueID PLAY_AMB_ROOMTONE_HALL = 460548394U;
        static const AkUniqueID PLAY_AMB_ROOMTONE_HOSPTITAL = 1049199867U;
        static const AkUniqueID PLAY_AMB_ROOMTONE_PLAYROOM = 941023664U;
        static const AkUniqueID PLAY_MUSIC_GAME = 637501428U;
        static const AkUniqueID PLAY_SFX_ASH_ENTERING = 3047501943U;
        static const AkUniqueID PLAY_SFX_ASH_ENTRANCE = 3490102551U;
        static const AkUniqueID PLAY_SFX_ASH_FOOTSTEPS = 3458164030U;
        static const AkUniqueID PLAY_SFX_ASH_MOVEMENT = 2037460026U;
        static const AkUniqueID PLAY_SFX_GAME_GAME_OVER = 3259796238U;
        static const AkUniqueID PLAY_SFX_GAME_VICTORY = 1384844463U;
        static const AkUniqueID PLAY_SFX_INT_BALLOON_BURST = 129274774U;
        static const AkUniqueID PLAY_SFX_INT_BOOK_FALL_ON_FLOOR = 3532855204U;
        static const AkUniqueID PLAY_SFX_INT_BUTTON_PUSH_DOORKEY = 3622062475U;
        static const AkUniqueID PLAY_SFX_INT_BUTTON_PUSH_LEVER = 1818531362U;
        static const AkUniqueID PLAY_SFX_INT_CALENDAR_MOVING_PAGES = 133490922U;
        static const AkUniqueID PLAY_SFX_INT_CHARENTAISE_TOSS_IMPACT = 3942372434U;
        static const AkUniqueID PLAY_SFX_INT_DENTURE_TOSS_IMPACT = 2205318028U;
        static const AkUniqueID PLAY_SFX_INT_DISHES_BREAK_LONG = 1306945837U;
        static const AkUniqueID PLAY_SFX_INT_DISHES_BREAK_RIDE_ON = 1890814399U;
        static const AkUniqueID PLAY_SFX_INT_DISHES_BREAK_SHORT = 2469250731U;
        static const AkUniqueID PLAY_SFX_INT_DOUBLE_DOOR_HALL_CLOSE = 3376709193U;
        static const AkUniqueID PLAY_SFX_INT_DOUBLE_DOOR_HALL_OPEN = 188723483U;
        static const AkUniqueID PLAY_SFX_INT_DRUG_ENERGIC_MOUVEMENT = 3737609047U;
        static const AkUniqueID PLAY_SFX_INT_DRUG_SHAKE = 4234958161U;
        static const AkUniqueID PLAY_SFX_INT_DRUG_SMOOTH_MOUVEMENT = 2528992456U;
        static const AkUniqueID PLAY_SFX_INT_DUCK_CLICK_OFF = 2557380336U;
        static const AkUniqueID PLAY_SFX_INT_DUCK_CLICK_ON = 3088274650U;
        static const AkUniqueID PLAY_SFX_INT_DUCK_VIBRATION_LOOP = 262351497U;
        static const AkUniqueID PLAY_SFX_INT_DUMBBELL_FALL = 1554269881U;
        static const AkUniqueID PLAY_SFX_INT_EXIT_DOOR_OPEN = 1568379754U;
        static const AkUniqueID PLAY_SFX_INT_EXTINGUISHER_SPLASH = 3313655593U;
        static const AkUniqueID PLAY_SFX_INT_GAMEROOM_EXIT_DOOR_CLOSE = 580606040U;
        static const AkUniqueID PLAY_SFX_INT_GAMEROOM_EXIT_DOOR_OPEN = 738240028U;
        static const AkUniqueID PLAY_SFX_INT_JEWEL_BOX_HELD = 3411232499U;
        static const AkUniqueID PLAY_SFX_INT_JEWEL_INBOX = 4212636704U;
        static const AkUniqueID PLAY_SFX_INT_JEWEL_SNAP_FINGER_PLAYER = 2409652336U;
        static const AkUniqueID PLAY_SFX_INT_LAMP_OFF = 2250335168U;
        static const AkUniqueID PLAY_SFX_INT_LAMP_ON = 735538570U;
        static const AkUniqueID PLAY_SFX_INT_PICK_EXTINGUISHER = 519695663U;
        static const AkUniqueID PLAY_SFX_INT_PICK_OBJECT_BASIC = 2655549148U;
        static const AkUniqueID PLAY_SFX_INT_PICK_OBJECT_PLASTIC = 723885614U;
        static const AkUniqueID PLAY_SFX_INT_SHOWER_WATER_FLOW = 58348137U;
        static const AkUniqueID PLAY_SFX_INT_SIMPLE_DOOR_CLOSE = 2276686680U;
        static const AkUniqueID PLAY_SFX_INT_SIMPLE_DOOR_OPEN = 518641436U;
        static const AkUniqueID PLAY_SFX_INT_TOILET_FLUSH = 1519471916U;
        static const AkUniqueID PLAY_SFX_INT_TV_EMISSION = 3801058192U;
        static const AkUniqueID PLAY_SFX_INT_TV_NOISE = 1494240985U;
        static const AkUniqueID PLAY_SFX_INT_TV_OFF = 2136677172U;
        static const AkUniqueID PLAY_SFX_INT_TV_ON = 4016263854U;
        static const AkUniqueID PLAY_SFX_MOVEMENT = 19004257U;
        static const AkUniqueID PLAY_SFX_TOSS_IMPACT_OBJECT_BALLOON = 1705819274U;
        static const AkUniqueID PLAY_SFX_TOSS_IMPACT_OBJECT_MEDIUM = 1886563500U;
        static const AkUniqueID PLAY_SFX_TOSS_IMPACT_OBJECT_SLIGHT = 2992695972U;
        static const AkUniqueID PLAY_SFX_TOSS_IMPACT_OBJECT_SOLID = 1138358578U;
        static const AkUniqueID PLAY_SFX_WHEELCHAIR_ROLL = 3584639996U;
        static const AkUniqueID PLAY_SFX_WHEELCHAIR_ROLL_BACKWARD = 741074560U;
        static const AkUniqueID PLAY_SFX_WHEELCHAIR_TURN = 1497278144U;
        static const AkUniqueID PLAY_SFX_WHEELCHAIR_WHEEL_GRAB = 2276431807U;
        static const AkUniqueID PLAY_SFX_WOOSH_HAND_TOSS = 2589173568U;
        static const AkUniqueID PLAY_UI_MENU_BUTTON_BACK = 1614015457U;
        static const AkUniqueID PLAY_UI_MENU_BUTTON_CLICK = 2301910688U;
        static const AkUniqueID PLAY_UI_MENU_BUTTON_HOVER = 1883557774U;
        static const AkUniqueID PLAY_UI_TINDOLD_APP_STARTING_OPENING = 1996915587U;
        static const AkUniqueID PLAY_UI_TINDOLD_BUTTON_CLICK = 2388394475U;
        static const AkUniqueID PLAY_UI_TINDOLD_BUTTON_CRUSHING = 4191105558U;
        static const AkUniqueID PLAY_UI_TINDOLD_BUTTON_HOVER = 906599893U;
        static const AkUniqueID PLAY_UI_TINDOLD_BUTTON_NOT_CRUSHING = 1128567860U;
        static const AkUniqueID PLAY_UI_TINDOLD_SWIPE = 945137512U;
        static const AkUniqueID PLAY_VOICE_ASH_ALERTE = 2600264923U;
        static const AkUniqueID PLAY_VOICE_ASH_CATCH = 2034014333U;
        static const AkUniqueID PLAY_VOICE_ASH_CHASE = 1857639658U;
        static const AkUniqueID PLAY_VOICE_ASH_SURPRISE = 1957295185U;
        static const AkUniqueID PLAY_VOICE_GRANDMA_VICTORY = 862924974U;
        static const AkUniqueID RESUME_ALL = 3679762312U;
        static const AkUniqueID SET_SWITCH_SURFACE_01 = 1145865760U;
        static const AkUniqueID SET_SWITCH_SURFACE_02 = 1145865763U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace STATE
            {
                static const AkUniqueID MUSIC_CHASE = 2675663049U;
                static const AkUniqueID MUSIC_ENDGAME = 4020650282U;
                static const AkUniqueID MUSIC_THEME = 733800440U;
                static const AkUniqueID MUSIC_VICTORY = 2637400697U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace MUSIC

    } // namespace STATES

    namespace SWITCHES
    {
        namespace SFX_TV_NOISE
        {
            static const AkUniqueID GROUP = 1634821240U;

            namespace SWITCH
            {
                static const AkUniqueID NO_SIGNAL = 1774699887U;
                static const AkUniqueID SIGNAL = 888557005U;
            } // namespace SWITCH
        } // namespace SFX_TV_NOISE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AMBIANCE_VOLUME = 4287144566U;
        static const AkUniqueID DRUG_BOTTLE_PILL_MOVEMENT = 3277326164U;
        static const AkUniqueID MASTER_VOLUME = 4179668880U;
        static const AkUniqueID MUSIC_VOLUME = 1006694123U;
        static const AkUniqueID POWER_INTENSITY = 3644539126U;
        static const AkUniqueID PUBLIC_SIZE = 650733778U;
        static const AkUniqueID REVERB_VOLUME = 4143275766U;
        static const AkUniqueID RTPC_PLAYSPEED = 2189383914U;
        static const AkUniqueID RTPC_REVERB_OUTDOOR = 3291249722U;
        static const AkUniqueID RTPC_VELOCITY = 1851181158U;
        static const AkUniqueID SFX_VOLUME = 1564184899U;
        static const AkUniqueID VIEW = 3235954734U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID DATENIGHT = 2358686833U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIANCE = 2981377429U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID REVERB = 348963605U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID UI = 1551306167U;
        static const AkUniqueID VOICE = 3170124113U;
        static const AkUniqueID VOICE_ASH = 589186428U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID RVB_CHAMBER = 1682361562U;
        static const AkUniqueID RVB_CORRIDOR = 3148728066U;
        static const AkUniqueID RVB_HALL = 4180545973U;
        static const AkUniqueID RVB_HOSPTITAL = 2603992486U;
        static const AkUniqueID RVB_OUTDOOR = 1883919640U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
